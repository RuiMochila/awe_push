## Installation

Add this line to your application's Gemfile:

    gem 'awe_push'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install awe_push

## Usage

### Rails Generator
Generate the AwePush initializer if you are using Ruby on Rails.

    $ rails g awe_push:install

### API Client

The easiest way to use the API client is to set the server `auth_token` at the module level and call methods on the AwePush module. You can find the token on settings page for your app.

```ruby
  AwePush.auth_token = 'prod_your-server-token'
```

You may then instantiate clients by calling the method that matches the auth token key:

```ruby
  AwePush.broadcast( ... )
```


Lastly, if you have many apps you may instantiate clients API Clients

```ruby
  client_1 = AwePush.client('iosprod_app-server-token-1')
  client_1.broadcast(alert: 'hello, app1')

  client_2 = AwePush.client('iosprod_app-server-token-2')
  client_1.broadcast(alert: 'hello, app2')
```

Methods supported by this gem and their parameters can be found in the [API Reference](#)
