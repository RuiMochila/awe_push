# require 'awe_push/compatibility'
require 'faraday_middleware'

module AwePush
  class Client
    URL = 'https://awepush.herokuapp.com'.freeze

    attr_accessor :auth_token #, :environment

    def initialize(auth_token) # , environment
      self.auth_token = auth_token
      #self.environment = environment

      # self.extend(Compatibility)
    end

    # verifies credentials
    #
    # @return [Boolean]
    def verify_credentials
      response = http.get('/verify_credentials')
      response.status == 200
    end

    # Sends a notification to the list of devices
    #
    # @param params [Hash]
    #
    # Example response
    # {"sent_count":10,"inactive_tokens":[],"unregistered_tokens":["abc"]}
    def notify(params) # Not supported
      http.post('/notify', params)
    end

    # Sends a notification to all of the devices registered with the AwePush backend
    #
    # @param params [Hash]
    #
    # Example response
    # {"sent_count":10}
    def broadcast(params) # Ill work no this one.
      params.merge!(server_auth_token: self.auth_token)
      # params.merge!(environment: self.environment)
      http.post('/api/v1/broadcast', params)
    end

    # Subscribes a device to a particular notification channel
    #
    # @param device_token [String]
    # @param channel      [String]
    #
    # Example response
    # {"device_token":"abc", "channels":["foo"]}
    def subscribe(device_token, channel) # Not supported
      http.post("/subscribe/#{channel}", device_token:device_token)
    end

    # Unsubscribes a device from a particular notification channel
    #
    # @param device_token [String]
    # @param channel      [String]
    #
    # Example response
    # {"device_token":"abc", "channels":[]}
    def unsubscribe(device_token, channel) # Not supported
      http.delete("/subscribe/#{channel}", device_token:device_token)
    end

    # Registers a device token with the AwePush backend
    #
    # @param device_token
    #
    # Example response
    # {"message":"ok"}
    def register(device_token, channel=nil) # Not supported
      params = {device_token: device_token}
      params.merge!(channel: channel) unless channel.nil?
      http.post('/register', params)
    end

    # Unregisters a device token that has previously been registered with
    #
    # @param device_token
    #
    # Example response
    # {"message":"ok"}
    def unregister(device_token) # Not supported
      http.delete('/unregister', device_token: device_token)
    end

    # Sets the badge for a particular device
    #
    # @param device_token
    # @param badge
    #
    # Example response
    # {"message":"ok"}
    def set_badge(device_token, badge) # Not supported
      http.post('/set_badge', device_token: device_token, badge: badge)
    end

    # Returns a list of tokens that have been marked inactive
    #
    # Example response
    # [
    #   {
    #     "device_token":"238b8cb09011850cb4bd544dfe0c8f5eeab73d7eeaae9bdca59076db4ae49947",
    #     "marked_inactive_at":"2013-07-17T01:27:53-04:00"
    #   },
    #   {
    #     "device_token":"8c97be6643eea2143322005bc4c44a1aee5e549bce5e2bb2116114f45484ddaf",
    #     "marked_inactive_at":"2013-07-17T01:27:50-04:00"
    #   }
    # ]
    def inactive_tokens(params = {page:1}) # Not supported
      http.get('/inactive_tokens', params)
    end

    # Returns a paginated list of devices
    #
    # Example response
    # [
    #   {
    #     "token": "1234567890abcdef1234567890abcdef1234567890abcdef1234567890abcedf",
    #     "active": true,
    #     "marked_inactive_at": null,
    #     "badge": 1
    #   },
    #   {
    #     "token": "234567890abcdef1234567890abcdef1234567890abcdef1234567890abcedf0",
    #     "active": true,
    #     "marked_inactive_at": null,
    #     "badge": 2
    #   }
    # ]
    def devices(params = {page:1}) # Not supported
      http.get('/devices', params)
    end

    # Return detailed information about a device
    #
    # Example response
    # {
    #   "token": "1234567890abcdef1234567890abcdef1234567890abcdef1234567890abcedf",
    #   "active": true,
    #   "marked_inactive_at": null,
    #   "badge": 1,
    #   "channels": [
    #     "testflight",
    #     "user@example.com"
    #   ]
    # }
    def device(token) # Not supported
      http.get("/devices/#{token}")
    end

    # Replace the channel subscriptions with a new set of channels. This will
    # remove all previous subscriptions of the device. If you want to append a
    # list of channels, use #update_device.
    #
    # @param token        String  token identifying the device
    # @param channel_list String  Comma separated list of channels
    #
    # Example Request
    #
    # AwePush.set_device(token, channel_list: 'player-1, game-256')
    #
    # Example Response
    # {
    #   "token": "1234567890abcdef1234567890abcdef1234567890abcdef1234567890abcedf",
    #   "active": true,
    #   "marked_inactive_at": null,
    #   "badge": 1,
    #   "channels": [
    #     "player-1",
    #     "game-256"
    #   ]
    # }
    def set_device(token, params) # Not supported
      http.put("/devices/#{token}", params)
    end

    # Append the channel subscriptions with a set of new channels. If you want
    # to replace the list of channels, use #set_device.
    #
    # @param token        String  token identifying the device
    # @param channel_list String  Comma separated list of channels
    #
    # Example Request
    #
    # AwePush.update_device(token, channel_list: 'player-1, game-256')
    #
    # Example Response
    # {
    #   "token": "1234567890abcdef1234567890abcdef1234567890abcdef1234567890abcedf",
    #   "active": true,
    #   "marked_inactive_at": null,
    #   "badge": 1,
    #   "channels": [
    #     "player-1",
    #     "game-256"
    #   ]
    # }
    def update_device(token, params) # Not supported
      http.patch("/devices/#{token}", params)
    end

    # Returns paginated list of channels
    #
    # Example Response:
    # [
    #   "player-1",
    #   "player-2",
    #   "player-9",
    #   "game-256",
    #   "admins",
    #   "lobby"
    # ]
    def channels(params = {page:1}) # Not supported
      http.get('/channels', params)
    end

    # Returns the list of device tokens for the given channel
    #
    # Example Response:
    # {
    #   "channel": "player-1",
    #   "device_tokens": [
    #     "1234567890abcdef1234567890abcdef1234567890abcdef1234567890abcedf"
    #   ]
    # }
    def channel(channel_name) # Not supported
      http.get("/channels/#{channel_name}")
    end

    # Deletes a channels and unsubscribes all of the devices from it.
    #
    # {
    #   "channel": "player-1",
    #   "device_tokens": [
    #     "1234567890abcdef1234567890abcdef1234567890abcdef1234567890abcedf"
    #   ]
    # }
    def delete_channel(channel_name) # Not supported
      http.delete("/channels/#{channel_name}")
    end

    # Instantiate a new http client configured for making requests to the API
    def http
      Faraday.new(url: URL) do |c|
        # c.token_auth self.auth_token # Check this later
        c.request    http_config[:request_encoding]
        c.response   :json, :content_type => /\bjson$/ # parse responses to JSON
        c.adapter    http_config[:http_adapter]
      end
    end
    alias client http

    protected
      def http_config
        @http_config ||= AwePush.config.dup
      end
  end
end
