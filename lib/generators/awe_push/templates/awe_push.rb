if Rails.env.production?
  AwePush.auth_token = '<%= @production_token %>'
else
  AwePush.auth_token = '<%= @development_token %>'
end
